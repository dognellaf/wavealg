﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchMov : MonoBehaviour
{
    public Rigidbody Player;
    public JoystickMovement Joy;
    public float Speed;
    void FixedUpdate()
    {
        Player.velocity = Vector3.zero;
        Player.AddForce(Joy.HorizontalInput() * Speed, Joy.VerticalInput() * Speed, 0);
    }
}
