﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

[CustomEditor(typeof(WaveAlg))]
public class WaveAlgGUI : Editor
{
    public override void OnInspectorGUI()
    {
        WaveAlg CFS = (WaveAlg)target;

        if (GUILayout.Button("Построить доску"))
        {
            CFS.StartFunc();
        }

        if (GUILayout.Button("Построить границы вокруг доски"))
        {
            CFS.CreateBorders();
        }

        if (GUILayout.Button("Очистить доску"))
        {
            CFS.ClearDesk();
        }

        //if (GUILayout.Button("Удалить доску"))
        //{
        //    CFS.DestroyDesk();
        //}

        if (GUILayout.Button("Выполнить следующий шаг"))
        {
            if (CFS.CurrentNumber == 0)
            {
                CFS.CheckNeighboor(CFS.StartPoint);
                CFS.CurrentNumber++;
            }
            else
                CFS.FindLast();
        }

        GUILayout.Label("");
        GUILayout.Label("Основная часть скрипта:");
        GUILayout.Label("");
        DrawDefaultInspector();
    }
}
