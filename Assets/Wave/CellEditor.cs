﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(CellFunc))]
public class CellEditor : Editor
{
    public override void OnInspectorGUI()
    {
        CellFunc CFS = (CellFunc)target;

        if (GUILayout.Button("Поставить левую стену"))
        {
            CFS.LeftBorder.SetActive(true);
            CFS.C.Walls[3] = true;
        }
            
        if (GUILayout.Button("Поставить нижнюю стену"))
        {
            CFS.DownBorder.SetActive(true);
            CFS.C.Walls[2] = true;
        }
            
        if (GUILayout.Button("Поставить верхнюю стену"))
        {
            CFS.UpBorder.SetActive(true);
            CFS.C.Walls[0] = true;
        }
            
        if (GUILayout.Button("Поставить правую стену"))
        {
            CFS.RightBorder.SetActive(true);
            CFS.C.Walls[1] = true;
        }
            

        if (GUILayout.Button("Убрать стену"))
        {
            if (CFS.Size == string.Empty)
            {
                Debug.LogError("Введите все значения в поля!");
            }
            else
            {
                switch (int.Parse(CFS.Size))
                {
                    case 0:
                        CFS.UpBorder.SetActive(false);
                        break;
                    case 1:
                        CFS.RightBorder.SetActive(false);
                        break;
                    case 2:
                        CFS.DownBorder.SetActive(false);
                        break;
                    case 3:
                        CFS.LeftBorder.SetActive(false);
                        break;
                }
                CFS.C.DestroyWall(byte.Parse(CFS.Size));
            }
        }
        GUILayout.Label("");
        GUILayout.Label("Основная часть скрипта:");
        GUILayout.Label("");
        DrawDefaultInspector();

    }

}
