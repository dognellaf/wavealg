﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using System.Xml.Serialization;

public class WaveAlg : MonoBehaviour
{
    public Vector2 StartPoint, EndPoint;
    public int Size;
    public int CurrentNumber = 0;
    public Cell[,] Cells;
    public void StartFunc()
    {
        Cells = GetComponent<CreateDesk>().CD(Size, StartPoint, EndPoint);
    }

    public void SubCheck(Vector2 V, int LocalPos)
    {
        Cell C = Cells[(int)(V.x), (int)(V.y)];
        C.SubText.text = LocalPos.ToString();
        C.Number = LocalPos;
    }

    public bool CheckInt(Vector2 V, float pos)
    {
        bool flag1 = pos >= 0;
        bool flag2 = pos < Size;
        bool flag3, flag4;

        if (flag1 && flag2)
        {
            Cell C = Cells[(int)(V.x), (int)(V.y)];
            flag3 = C.Number == -1;
        }
        else
            return false;
        
        if (flag1 && flag2 && flag3)
            return true;
        else
            return false;
    }

    public void CheckNeighboor(Vector2 _point)
    {
        int LocalPos = CurrentNumber + 1;
        try
        {
            Debug.Log(string.Format("Первый: {1}, второй: {0}", Cells[(int)(_point.x), (int)(_point.y)].Walls[3], CheckInt(new Vector2(_point.x - 1, _point.y), _point.x - 1)));
        }
        finally { }
        
        if (CheckInt(new Vector2(_point.x - 1, _point.y), _point.x - 1) && (Cells[(int)(_point.x), (int)(_point.y)].Walls[3] == false))
            SubCheck(new Vector2(_point.x - 1, _point.y), LocalPos);

        if (CheckInt(new Vector2(_point.x + 1, _point.y), _point.x + 1) && (Cells[(int)(_point.x), (int)(_point.y)].Walls[1] == false))
            SubCheck(new Vector2(_point.x + 1, _point.y), LocalPos);

        if (CheckInt(new Vector2(_point.x, _point.y - 1), _point.y - 1) && (Cells[(int)(_point.x), (int)(_point.y)].Walls[0] == false))
            SubCheck(new Vector2(_point.x, _point.y - 1), LocalPos);

        if (CheckInt(new Vector2(_point.x, _point.y + 1), _point.y + 1) && (Cells[(int)(_point.x), (int)(_point.y)].Walls[2] == false))
            SubCheck(new Vector2(_point.x, _point.y + 1), LocalPos);
    }

    public void FindLast()
    {
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                if (Cells[x, y].Number == CurrentNumber)
                {
                    CheckNeighboor(new Vector2(x, y));
                }
            }
        }
        CurrentNumber++;
    }

    public void ClearDesk()
    {
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                Cells[x, y].Number = -1;
                Cells[x, y].SubText.text = string.Empty;
            }
        }
        CurrentNumber++;
    }

    public void CreateBorders()
    {
        for (int x = 0; x < Size; x++)
        {
            for (int y = 0; y < Size; y++)
            {
                if (y == 0)
                {
                    Cells[x, y].CellObj.GetComponent<CellFunc>().UpBorder.gameObject.SetActive(true);
                    Cells[x, y].SetWall(0);
                }
                if (x == 0)
                {
                    Cells[x, y].CellObj.GetComponent<CellFunc>().LeftBorder.gameObject.SetActive(true);
                    Cells[x, y].SetWall(3);
                }
                if (x == Size - 1)
                {
                    Cells[x, y].CellObj.GetComponent<CellFunc>().RightBorder.gameObject.SetActive(true);
                    Cells[x, y].SetWall(1);
                }
                if (y == Size - 1)
                {
                    Cells[x, y].CellObj.GetComponent<CellFunc>().DownBorder.gameObject.SetActive(true);
                    Cells[x, y].SetWall(2);
                }
            }
        }
    }

    //public void DestroyDesk()
    //{
    //    for (int x = 0; x < Size; x++)
    //    {
    //        for (int y = 0; y < Size; y++)
    //        {
    //            Cells[x, y].CellObj.SetActive(false);
    //            Cells[x, y] = null;
    //        }
    //    }
    //    CurrentNumber = 0;
    //}

}

public class Cell
{
    public Text SubText;
    public int Number = -1;
    public bool[] Walls;
    public GameObject CellObj;

    public Cell(GameObject _prefab, GameObject Cv)
    {
        Walls = new bool[4] { false, false, false, false }; //up, right, down, left
        CellObj = GameObject.Instantiate(_prefab, Cv.transform);
        SubText = CellObj.transform.Find("Text").GetComponent<Text>();
    }

    public void SetWall(byte side) => Walls[side] = true;
    public void DestroyWall(byte size) => Walls[size] = false;

}