﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine.UI;
using UnityEngine;

public class CreateDesk : MonoBehaviour
{
    public GameObject CellPrefab;
    public Sprite StartPointImg, EndPointImg;
    public GameObject SubCv;
    public Cell[,] CD(int Size, Vector2 StartPoint, Vector2 EndPoint)
    {
        Cell[,] Cells;
        Cells = new Cell[Size, Size];
        for (int y = 0; y < Size; y++)
        {
            for (int x = 0; x < Size; x++)
            {

                //создаем объект ячейки
                Cells[x, y] = new Cell(CellPrefab, SubCv);
                Cells[x, y].CellObj.GetComponent<CellFunc>().WA = GetComponent<WaveAlg>();
                Cells[x, y].CellObj.transform.localPosition = new Vector3(x * 100, -y * 100, 0);
                Cells[x, y].CellObj.name = string.Format("Cell[{0}, {1}]", x, y);

                
                if ((x == StartPoint.x)&&(y == StartPoint.y))
                {
                    Cells[x, y].Number = 0;
                    Cells[x, y].CellObj.transform.Find("Text").GetComponent<Text>().text = "0";
                    Cells[x, y].CellObj.transform.Find("image").GetComponent<Image>().sprite = StartPointImg; //устанавливаем текстуру, если начальная позиция
                }
                else if ((x == EndPoint.x) && (y == EndPoint.y))
                    Cells[x, y].CellObj.transform.Find("image").GetComponent<Image>().sprite = EndPointImg; //устанавливаем текстуру, если конечная позиция
            }
        }
        return Cells;
    }
}
