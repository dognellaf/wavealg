﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;


public class CellFunc : MonoBehaviour
{
    public Cell C;
    public WaveAlg WA;
    public string Size;
    public GameObject LeftBorder;
    public GameObject RightBorder;
    public GameObject UpBorder;
    public GameObject DownBorder;
    void Start()
    {
        Regex regex1 = new Regex(@"\d(\w*)");
        MatchCollection matches = regex1.Matches(name);
        int x = int.Parse(matches[0].Value);
        int y = int.Parse(matches[1].Value);
        C = WA.Cells[x, y];
    }
}