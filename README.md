## Визуализация волнового алгоритма.

Этот проект позволяет визуализировать волновой алгоритм в Unity с использованием собственного редактора.
Скриншоты примера:
![alt text](./Example/1.png)
![alt text](./Example/2.gif)
![alt text](./Example/3.gif)